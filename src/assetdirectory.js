export let AssetDirectory = {
	"load": [
		"assets/fonts/Air Americana.ttf",
		"assets/fonts/AirAmericana.eot",
		"assets/fonts/AirAmericana.svg",
		"assets/fonts/AirAmericana.woff",
		"assets/fonts/AirAmericana.woff2",
		"assets/images/Asteroid.png",
		"assets/images/Coin.png",
		"assets/images/SmallPac.png"
	],
	"audio": [
		"assets/audio/BGM.mp3",
		"assets/audio/Boing.mp3",
		"assets/audio/PewSFX.mp3",
		"assets/audio/Punch.mp3",
		"assets/audio/WallE.mp3"
	]
};