import { Container, Text,TEXT_GRADIENT,ticker,utils,Sprite } from "pixi.js";
import KeyCodes from "../KeyCodes";
import KeyValues from "../KeyValues";

class TitleScreen extends Container{
    constructor(){
        super();
        this.playingBGM = true;
        this.wordArray = [];

        var cache = utils.TextureCache;
        this.image = new Sprite(cache["assets/images/Coin.png"]);
        this.image2 = new Sprite(cache["assets/images/SmallPac.png"]);
        this.image2.x = 500;
        this.image2.y = 400;

        this.word = new PIXI.Text('',{
            fontFamily: 'Arial',
            fontSize: 35,
            fill: 0x00FFFF,
            align: 'center'
        });
        this.word.position.set(200,150);

        this.numbText = new PIXI.Text('Hello World',{
            fontFamily: 'Arial',
            fontSize: 35,
            fill: 0x00FFFF,
            align: 'center'
        });
        this.numbText.position.set(450,50);

        this.tutorText = new PIXI.Text('arrow keys to move coin,1-to mute bgm, 2orMouseclick-Play punch sfx, 3-play boing sfx, 4 or click SmallPacman-stop or playbgm',{
            fontFamily: 'Arial',
            fontSize: 15,
            fill: 0x00FFFF,
            align: 'center'
        });
        this.tutorText.position.set(0,450);

        //Number5
        this.bgmInstance = createjs.Sound.play("assets/audio/WallE.mp3",{
            volume : 1.0,
            loop : -1
            //delay: 100 //ma
        })

        this.sfxInstance1 = createjs.Sound.play("assets/audio/Punch.mp3",{
            volume : 1.0,
            loop : 0
            //delay: 100 //ma
        })

        this.sfxInstance2 = createjs.Sound.play("assets/audio/Boing.mp3",{
            volume : 1.0,
            loop : 0
            //delay: 100 //ma
        })

        this.image2.interactive = true;
        this.image2.on('pointerdown',this.onClick.bind(this));

        this.addChild(this.numbText);
        this.addChild(this.word);
        this.addChild(this.tutorText);
        this.addChild(this.image);
        this.addChild(this.image2);

        window.addEventListener("keyup",this.onKeyUp.bind(this));
        window.addEventListener("keydown",this.onKeyDown.bind(this));
        window.addEventListener("mousedown",this.mouseDown.bind(this));
    }

    onClick(a){
        //Number9
        //this.bgmInstance.play = !this.bgmInstance.stop;
        console.log("clicked");
        if(this.playingBGM)
        {
            this.bgmInstance.stop();
            this.playingBGM = false;
        }
        else if(!this.playingBGM)
        {
            this.bgmInstance.play();
            this.playingBGM = true;
        }
    
    }
    mouseDown(m){
        //Number 7
        this.sfxInstance1.play();
    }

    onKeyUp(e){
        //Number 1
        this.numbText.text = e.key;
        //Number3
        if(e.key == "Shift")
        {
            return;
        }
        this.wordArray.push(e.key);
        //Number4 for some reason the pop or splice is not working
        if(e.key == "Backspace")
        {
            this.wordArray.pop();
            console.log("b = "+this.wordArray);
            this.word.text = this.wordArray.join("");
            console.log("a = "+this.wordArray);
        }
        console.log("b if = "+this.wordArray);
        this.word.text = this.wordArray.join("");
        console.log("a if = "+this.wordArray);
        //Number6
        if(e.key == "1")
        {
            this.bgmInstance.muted = !this.bgmInstance.muted;
        }
        if(e.key == "2")
        {
            this.sfxInstance1.play();
        }
        if(e.key == "3")
        {
            this.sfxInstance2.play();
        }
        //Number 8
        if(e.key == "4")
        {
            if(this.playingBGM)
            {
                this.bgmInstance.stop();
                this.playingBGM = false;
            }
            else if(!this.playingBGM)
            {
                this.bgmInstance.play();
                this.playingBGM = true;
            }
        }
    }

    onKeyDown(a){
        //Number2
        this.moveObj(this.image,a);
    }

    moveObj(obj,a){
        if(a.key == "ArrowUp")
        {
            obj.y -= 5;
        }
        else if(a.key == "ArrowDown")
        {
            obj.y += 5;
        }
        else if(a.key == "ArrowLeft")
        {
            obj.x -= 5;
        }
        else if(a.key == "ArrowRight")
        {
            obj.x += 5;
        }
        if(obj.x >= 550)
        {
            obj.x = 550;
        }
        if(obj.x <= 0)
        {
            obj.x = 0;
        }
        if(obj.y >= 400)
        {
            obj.y = 400;
        }
        if(obj.y <= 0)
        {
            obj.y = 0;
        }
    }
}

export default TitleScreen;