/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/index.js","vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/KeyCodes.js":
/*!*************************!*\
  !*** ./src/KeyCodes.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nvar KeyCodes = {\n    KeySpace: 32,\n    KeyA: 65,\n    KeyB: 66,\n    KeyC: 67,\n    KeyD: 68,\n    KeyE: 69,\n    KeyF: 70,\n    KeyG: 71,\n    KeyH: 72,\n    KeyI: 73,\n    KeyJ: 74,\n    KeyK: 75,\n    KeyL: 76,\n    KeyM: 77,\n    KeyN: 78,\n    KeyO: 79,\n    KeyP: 80,\n    KeyQ: 81,\n    KeyR: 82,\n    KeyS: 83,\n    KeyT: 84,\n    KeyU: 85,\n    KeyV: 86,\n    KeyW: 87,\n    KeyX: 88,\n    KeyY: 89,\n    KeyZ: 90,\n    Key0: 48,\n    Key1: 49,\n    Key2: 50,\n    Key3: 51,\n    Key4: 52,\n    Key5: 53,\n    Key6: 54,\n    Key7: 55,\n    Key8: 56,\n    Key9: 57,\n    KeyNumPad0: 96,\n    KeyNumPad1: 97,\n    KeyNumPad2: 98,\n    KeyNumPad3: 99,\n    KeyNumPad4: 100,\n    KeyNumPad5: 101,\n    KeyNumPad6: 102,\n    KeyNumPad7: 103,\n    KeyNumPad8: 104,\n    KeyNumPad9: 105,\n    KeyArrowUp: 38,\n    KeyArrowDown: 40,\n    KeyArrowLeft: 37,\n    KeyArrowRight: 39,\n    BackSpace: 8\n};\n\nexports.default = KeyCodes;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvS2V5Q29kZXMuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL0tleUNvZGVzLmpzPzE3ZDMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIEtleUNvZGVzID0ge1xyXG4gICAgS2V5U3BhY2U6IDMyLFxyXG4gICAgS2V5QSA6IDY1LFxyXG4gICAgS2V5QiA6IDY2LFxyXG4gICAgS2V5QyA6IDY3LFxyXG4gICAgS2V5RCA6IDY4LFxyXG4gICAgS2V5RSA6IDY5LFxyXG4gICAgS2V5RiA6IDcwLFxyXG4gICAgS2V5RyA6IDcxLFxyXG4gICAgS2V5SCA6IDcyLFxyXG4gICAgS2V5SSA6IDczLFxyXG4gICAgS2V5SiA6IDc0LFxyXG4gICAgS2V5SyA6IDc1LFxyXG4gICAgS2V5TCA6IDc2LFxyXG4gICAgS2V5TSA6IDc3LFxyXG4gICAgS2V5TiA6IDc4LFxyXG4gICAgS2V5TyA6IDc5LFxyXG4gICAgS2V5UCA6IDgwLFxyXG4gICAgS2V5USA6IDgxLFxyXG4gICAgS2V5UiA6IDgyLFxyXG4gICAgS2V5UyA6IDgzLFxyXG4gICAgS2V5VCA6IDg0LFxyXG4gICAgS2V5VSA6IDg1LFxyXG4gICAgS2V5ViA6IDg2LFxyXG4gICAgS2V5VyA6IDg3LFxyXG4gICAgS2V5WCA6IDg4LFxyXG4gICAgS2V5WSA6IDg5LFxyXG4gICAgS2V5WiA6IDkwLFxyXG4gICAgS2V5MCA6IDQ4LFxyXG4gICAgS2V5MSA6IDQ5LFxyXG4gICAgS2V5MiA6IDUwLFxyXG4gICAgS2V5MyA6IDUxLFxyXG4gICAgS2V5NCA6IDUyLFxyXG4gICAgS2V5NSA6IDUzLFxyXG4gICAgS2V5NiA6IDU0LFxyXG4gICAgS2V5NyA6IDU1LFxyXG4gICAgS2V5OCA6IDU2LFxyXG4gICAgS2V5OSA6IDU3LFxyXG4gICAgS2V5TnVtUGFkMCA6IDk2LFxyXG4gICAgS2V5TnVtUGFkMSA6IDk3LFxyXG4gICAgS2V5TnVtUGFkMiA6IDk4LFxyXG4gICAgS2V5TnVtUGFkMyA6IDk5LFxyXG4gICAgS2V5TnVtUGFkNCA6IDEwMCxcclxuICAgIEtleU51bVBhZDUgOiAxMDEsXHJcbiAgICBLZXlOdW1QYWQ2IDogMTAyLFxyXG4gICAgS2V5TnVtUGFkNyA6IDEwMyxcclxuICAgIEtleU51bVBhZDggOiAxMDQsXHJcbiAgICBLZXlOdW1QYWQ5IDogMTA1LFxyXG4gICAgS2V5QXJyb3dVcCA6IDM4LFxyXG4gICAgS2V5QXJyb3dEb3duIDogNDAsXHJcbiAgICBLZXlBcnJvd0xlZnQgOiAzNyxcclxuICAgIEtleUFycm93UmlnaHQgOiAzOSxcclxuICAgIEJhY2tTcGFjZSA6IDhcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgS2V5Q29kZXM7Il0sIm1hcHBpbmdzIjoiOzs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFwREE7QUFDQTtBQXNEQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/KeyCodes.js\n");

/***/ }),

/***/ "./src/KeyValues.js":
/*!**************************!*\
  !*** ./src/KeyValues.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nvar KeyValues = {\n    32: \" \",\n    65: \"A\",\n    66: \"B\",\n    67: \"C\",\n    68: \"D\",\n    69: \"E\",\n    70: \"F\",\n    71: \"G\",\n    72: \"H\",\n    73: \"I\",\n    74: \"J\",\n    75: \"K\",\n    76: \"L\",\n    77: \"M\",\n    78: \"N\",\n    79: \"O\",\n    80: \"P\",\n    81: \"Q\",\n    82: \"R\",\n    83: \"S\",\n    84: \"T\",\n    85: \"U\",\n    86: \"V\",\n    87: \"W\",\n    88: \"X\",\n    89: \"Y\",\n    90: \"Z\",\n    48: \"0\",\n    49: \"1\",\n    50: \"2\",\n    51: \"3\",\n    52: \"4\",\n    53: \"5\",\n    54: \"6\",\n    55: \"7\",\n    56: \"8\",\n    57: \"9\",\n    96: \"0\",\n    97: \"1\",\n    98: \"2\",\n    99: \"3\",\n    100: \"4\",\n    101: \"5\",\n    102: \"6\",\n    103: \"7\",\n    104: \"8\",\n    105: \"9\",\n    37: \"ArrowLeft\",\n    38: \"ArrowUp\",\n    39: \"ArrowRight\",\n    40: \"ArrowDown \",\n    8: \"BackSpace\"\n\n};\n\nexports.default = KeyValues;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvS2V5VmFsdWVzLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9LZXlWYWx1ZXMuanM/NjcyOSJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgS2V5VmFsdWVzID0ge1xyXG4gICAgMzIgOiBcIiBcIixcclxuICAgIDY1IDogXCJBXCIsXHJcbiAgICA2NiA6IFwiQlwiLFxyXG4gICAgNjcgOiBcIkNcIixcclxuICAgIDY4IDogXCJEXCIsXHJcbiAgICA2OSA6IFwiRVwiLFxyXG4gICAgNzAgOiBcIkZcIixcclxuICAgIDcxIDogXCJHXCIsXHJcbiAgICA3MiA6IFwiSFwiLFxyXG4gICAgNzMgOiBcIklcIixcclxuICAgIDc0IDogXCJKXCIsXHJcbiAgICA3NSA6IFwiS1wiLFxyXG4gICAgNzYgOiBcIkxcIixcclxuICAgIDc3IDogXCJNXCIsXHJcbiAgICA3OCA6IFwiTlwiLFxyXG4gICAgNzkgOiBcIk9cIixcclxuICAgIDgwIDogXCJQXCIsXHJcbiAgICA4MSA6IFwiUVwiLFxyXG4gICAgODIgOiBcIlJcIixcclxuICAgIDgzIDogXCJTXCIsXHJcbiAgICA4NCA6IFwiVFwiLFxyXG4gICAgODUgOiBcIlVcIixcclxuICAgIDg2IDogXCJWXCIsXHJcbiAgICA4NyA6IFwiV1wiLFxyXG4gICAgODggOiBcIlhcIixcclxuICAgIDg5IDogXCJZXCIsXHJcbiAgICA5MCA6IFwiWlwiLFxyXG4gICAgNDggOiBcIjBcIixcclxuICAgIDQ5IDogXCIxXCIsXHJcbiAgICA1MCA6IFwiMlwiLFxyXG4gICAgNTEgOiBcIjNcIixcclxuICAgIDUyIDogXCI0XCIsXHJcbiAgICA1MyA6IFwiNVwiLFxyXG4gICAgNTQgOiBcIjZcIixcclxuICAgIDU1IDogXCI3XCIsXHJcbiAgICA1NiA6IFwiOFwiLFxyXG4gICAgNTcgOiBcIjlcIixcclxuICAgIDk2IDogXCIwXCIsXHJcbiAgICA5NyA6IFwiMVwiLFxyXG4gICAgOTggOiBcIjJcIixcclxuICAgIDk5IDogXCIzXCIsXHJcbiAgICAxMDAgOiBcIjRcIixcclxuICAgIDEwMSA6IFwiNVwiLFxyXG4gICAgMTAyIDogXCI2XCIsXHJcbiAgICAxMDMgOiBcIjdcIixcclxuICAgIDEwNCA6IFwiOFwiLFxyXG4gICAgMTA1IDogXCI5XCIsXHJcbiAgICAzNyA6IFwiQXJyb3dMZWZ0XCIsXHJcbiAgICAzOCA6IFwiQXJyb3dVcFwiLFxyXG4gICAgMzkgOiBcIkFycm93UmlnaHRcIixcclxuICAgIDQwIDogXCJBcnJvd0Rvd24gXCIsXHJcbiAgICA4OiBcIkJhY2tTcGFjZVwiLFxyXG5cclxuXHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBLZXlWYWx1ZXM7Il0sIm1hcHBpbmdzIjoiOzs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXJEQTtBQUNBO0FBeURBIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/KeyValues.js\n");

/***/ }),

/***/ "./src/Main.js":
/*!*********************!*\
  !*** ./src/Main.js ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nvar _assetdirectory = __webpack_require__(/*! ./assetdirectory */ \"./src/assetdirectory.js\");\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nvar _TitleScreen = __webpack_require__(/*! ./screen/TitleScreen */ \"./src/screen/TitleScreen.js\");\n\nvar _TitleScreen2 = _interopRequireDefault(_TitleScreen);\n\nvar _ = __webpack_require__(/*! . */ \"./src/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nvar Main = function () {\n    function Main() {\n        _classCallCheck(this, Main);\n    }\n\n    _createClass(Main, null, [{\n        key: \"start\",\n        value: function start() {\n            Main.cjAudioQueue = new createjs.LoadQueue();\n            createjs.Sound.alternateExtensions = [\"ogg\"];\n\n            Main.cjAudioQueue.installPlugin(createjs.Sound);\n            Main.cjAudioQueue.addEventListener(\"complete\", Main.handleAudioComplete.bind(Main));\n\n            if (_assetdirectory.AssetDirectory.audio.length > 0) {\n                //LOAD AUDIO                  \n                var audioFiles = _assetdirectory.AssetDirectory.audio;\n                var audioManifest = [];\n                for (var i = 0; i < audioFiles.length; i++) {\n                    audioManifest.push({\n                        id: audioFiles[i],\n                        src: audioFiles[i]\n                    });\n                }\n                Main.cjAudioQueue.loadManifest(audioManifest);\n            } else {\n                Main.handleAudioComplete();\n            }\n            //createjs.Sound.volume = 0.8;//Para 0.0-1.0\n            //createjs.Sound.muted = false;//\n        }\n    }, {\n        key: \"handleAudioComplete\",\n        value: function handleAudioComplete() {\n            if (_assetdirectory.AssetDirectory.load.length > 0) {\n                //LOAD IMAGES         \n                var loader = _pixi.loaders.shared;\n                loader.add(_assetdirectory.AssetDirectory.load);\n                loader.load(Main.handleImageComplete);\n            } else {\n                Main.handleImageComplete();\n            }\n        }\n    }, {\n        key: \"handleImageComplete\",\n        value: function handleImageComplete() {\n            var screen = new _TitleScreen2.default();\n            _.App.stage.addChild(screen);\n        }\n    }]);\n\n    return Main;\n}();\n\nexports.default = Main;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvTWFpbi5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9zcmMvTWFpbi5qcz8xMjIxIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFzc2V0RGlyZWN0b3J5IH0gZnJvbSBcIi4vYXNzZXRkaXJlY3RvcnlcIjtcclxuaW1wb3J0IHsgbG9hZGVycyB9IGZyb20gXCJwaXhpLmpzXCI7XHJcbmltcG9ydCBUaXRsZVNjcmVlbiBmcm9tIFwiLi9zY3JlZW4vVGl0bGVTY3JlZW5cIjtcclxuaW1wb3J0IHsgQXBwIH0gZnJvbSBcIi5cIjtcclxuXHJcblxyXG5jbGFzcyBNYWluIHtcclxuICAgIHN0YXRpYyBzdGFydCgpe1xyXG4gICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlID0gbmV3IGNyZWF0ZWpzLkxvYWRRdWV1ZSgpO1xyXG4gICAgICAgIGNyZWF0ZWpzLlNvdW5kLmFsdGVybmF0ZUV4dGVuc2lvbnMgPSBbXCJvZ2dcIl07XHJcblxyXG4gICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlLmluc3RhbGxQbHVnaW4oY3JlYXRlanMuU291bmQpO1xyXG4gICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlLmFkZEV2ZW50TGlzdGVuZXIoXCJjb21wbGV0ZVwiLCBNYWluLmhhbmRsZUF1ZGlvQ29tcGxldGUuYmluZChNYWluKSk7XHJcblxyXG4gICAgICAgIGlmKEFzc2V0RGlyZWN0b3J5LmF1ZGlvLmxlbmd0aCA+IDApe1xyXG4gICAgICAgICAgICAvL0xPQUQgQVVESU8gICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgbGV0IGF1ZGlvRmlsZXMgPSBBc3NldERpcmVjdG9yeS5hdWRpbztcclxuICAgICAgICAgICAgbGV0IGF1ZGlvTWFuaWZlc3QgPSBbXTtcclxuICAgICAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IGF1ZGlvRmlsZXMubGVuZ3RoOyBpKyspe1xyXG4gICAgICAgICAgICAgICAgYXVkaW9NYW5pZmVzdC5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogYXVkaW9GaWxlc1tpXSxcclxuICAgICAgICAgICAgICAgICAgICBzcmM6IGF1ZGlvRmlsZXNbaV1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlLmxvYWRNYW5pZmVzdChhdWRpb01hbmlmZXN0KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgTWFpbi5oYW5kbGVBdWRpb0NvbXBsZXRlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vY3JlYXRlanMuU291bmQudm9sdW1lID0gMC44Oy8vUGFyYSAwLjAtMS4wXHJcbiAgICAgICAgLy9jcmVhdGVqcy5Tb3VuZC5tdXRlZCA9IGZhbHNlOy8vXHJcblxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBoYW5kbGVBdWRpb0NvbXBsZXRlKCl7XHJcbiAgICAgICAgaWYoQXNzZXREaXJlY3RvcnkubG9hZC5sZW5ndGggPiAwKXtcclxuICAgICAgICAgICAgLy9MT0FEIElNQUdFUyAgICAgICAgIFxyXG4gICAgICAgICAgICBsZXQgbG9hZGVyID0gbG9hZGVycy5zaGFyZWQ7XHJcbiAgICAgICAgICAgIGxvYWRlci5hZGQoQXNzZXREaXJlY3RvcnkubG9hZCk7XHJcbiAgICAgICAgICAgIGxvYWRlci5sb2FkKE1haW4uaGFuZGxlSW1hZ2VDb21wbGV0ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICBNYWluLmhhbmRsZUltYWdlQ29tcGxldGUoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIGhhbmRsZUltYWdlQ29tcGxldGUoKXtcclxuICAgICAgICBsZXQgc2NyZWVuID0gbmV3IFRpdGxlU2NyZWVuKCk7XHJcbiAgICAgICAgQXBwLnN0YWdlLmFkZENoaWxkKHNjcmVlbik7XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IE1haW47Il0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTs7O0FBQUE7QUFDQTs7Ozs7QUFFQTs7Ozs7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFHQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/Main.js\n");

/***/ }),

/***/ "./src/assetdirectory.js":
/*!*******************************!*\
  !*** ./src/assetdirectory.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n\tvalue: true\n});\nvar AssetDirectory = exports.AssetDirectory = {\n\t\"load\": [\"assets/fonts/Air Americana.ttf\", \"assets/fonts/AirAmericana.eot\", \"assets/fonts/AirAmericana.svg\", \"assets/fonts/AirAmericana.woff\", \"assets/fonts/AirAmericana.woff2\", \"assets/images/Asteroid.png\", \"assets/images/Coin.png\", \"assets/images/SmallPac.png\"],\n\t\"audio\": [\"assets/audio/BGM.mp3\", \"assets/audio/Boing.mp3\", \"assets/audio/PewSFX.mp3\", \"assets/audio/Punch.mp3\", \"assets/audio/WallE.mp3\"]\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXNzZXRkaXJlY3RvcnkuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2Fzc2V0ZGlyZWN0b3J5LmpzPzZmZDEiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGxldCBBc3NldERpcmVjdG9yeSA9IHtcblx0XCJsb2FkXCI6IFtcblx0XHRcImFzc2V0cy9mb250cy9BaXIgQW1lcmljYW5hLnR0ZlwiLFxuXHRcdFwiYXNzZXRzL2ZvbnRzL0FpckFtZXJpY2FuYS5lb3RcIixcblx0XHRcImFzc2V0cy9mb250cy9BaXJBbWVyaWNhbmEuc3ZnXCIsXG5cdFx0XCJhc3NldHMvZm9udHMvQWlyQW1lcmljYW5hLndvZmZcIixcblx0XHRcImFzc2V0cy9mb250cy9BaXJBbWVyaWNhbmEud29mZjJcIixcblx0XHRcImFzc2V0cy9pbWFnZXMvQXN0ZXJvaWQucG5nXCIsXG5cdFx0XCJhc3NldHMvaW1hZ2VzL0NvaW4ucG5nXCIsXG5cdFx0XCJhc3NldHMvaW1hZ2VzL1NtYWxsUGFjLnBuZ1wiXG5cdF0sXG5cdFwiYXVkaW9cIjogW1xuXHRcdFwiYXNzZXRzL2F1ZGlvL0JHTS5tcDNcIixcblx0XHRcImFzc2V0cy9hdWRpby9Cb2luZy5tcDNcIixcblx0XHRcImFzc2V0cy9hdWRpby9QZXdTRlgubXAzXCIsXG5cdFx0XCJhc3NldHMvYXVkaW8vUHVuY2gubXAzXCIsXG5cdFx0XCJhc3NldHMvYXVkaW8vV2FsbEUubXAzXCJcblx0XVxufTsiXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUE7QUFDQTtBQVVBO0FBWEEiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/assetdirectory.js\n");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nexports.App = undefined;\n\nvar _assetdirectory = __webpack_require__(/*! ./assetdirectory.js */ \"./src/assetdirectory.js\");\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nvar _Main = __webpack_require__(/*! ./Main.js */ \"./src/Main.js\");\n\nvar _Main2 = _interopRequireDefault(_Main);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nvar Config = __webpack_require__(/*! Config */ \"Config\");\n\nvar canvas = document.getElementById('game-canvas');\nvar pixiapp = new _pixi.Application({\n    view: canvas,\n    width: Config.BUILD.WIDTH,\n    height: Config.BUILD.HEIGHT\n});\n\ndocument.body.style.margin = \"0px\";\ndocument.body.style.overflow = \"hidden\";\n\n/*****************************************\r\n ************* ENTRY POINT ***************\r\n *****************************************/\nfunction ready(fn) {\n    if (document.readyState != 'loading') {\n        fn();\n    } else {\n        document.addEventListener('DOMContentLoaded', fn);\n    }\n}\n\nready(function () {\n    _Main2.default.start();\n});\n\nexports.App = pixiapp;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2luZGV4LmpzPzEyZDUiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQXNzZXREaXJlY3RvcnkgfSBmcm9tICcuL2Fzc2V0ZGlyZWN0b3J5LmpzJztcclxuaW1wb3J0IHsgQXBwbGljYXRpb24gfSBmcm9tICdwaXhpLmpzJztcclxuaW1wb3J0IE1haW4gZnJvbSAnLi9NYWluLmpzJztcclxuXHJcbnZhciBDb25maWcgPSByZXF1aXJlKCdDb25maWcnKTtcclxuXHJcblxyXG5sZXQgY2FudmFzID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2dhbWUtY2FudmFzJyk7XHJcbmxldCBwaXhpYXBwID0gbmV3IEFwcGxpY2F0aW9uKHtcclxuICAgIHZpZXc6IGNhbnZhcyxcclxuICAgIHdpZHRoOiBDb25maWcuQlVJTEQuV0lEVEgsXHJcbiAgICBoZWlnaHQ6IENvbmZpZy5CVUlMRC5IRUlHSFRcclxufSlcclxuXHJcblxyXG5kb2N1bWVudC5ib2R5LnN0eWxlLm1hcmdpbiA9IFwiMHB4XCI7XHJcbmRvY3VtZW50LmJvZHkuc3R5bGUub3ZlcmZsb3cgPSBcImhpZGRlblwiO1xyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAqKioqKioqKioqKioqIEVOVFJZIFBPSU5UICoqKioqKioqKioqKioqKlxyXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbmZ1bmN0aW9uIHJlYWR5KGZuKSB7XHJcbiAgICBpZiAoZG9jdW1lbnQucmVhZHlTdGF0ZSAhPSAnbG9hZGluZycpIHtcclxuICAgICAgICBmbigpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgZm4pO1xyXG4gICAgfVxyXG59XHJcblxyXG5yZWFkeShmdW5jdGlvbiAoKSB7XHJcbiAgICBNYWluLnN0YXJ0KCk7XHJcbn0pO1xyXG5cclxuZXhwb3J0IHtwaXhpYXBwIGFzIEFwcH07Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBOzs7OztBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/index.js\n");

/***/ }),

/***/ "./src/screen/TitleScreen.js":
/*!***********************************!*\
  !*** ./src/screen/TitleScreen.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nvar _KeyCodes = __webpack_require__(/*! ../KeyCodes */ \"./src/KeyCodes.js\");\n\nvar _KeyCodes2 = _interopRequireDefault(_KeyCodes);\n\nvar _KeyValues = __webpack_require__(/*! ../KeyValues */ \"./src/KeyValues.js\");\n\nvar _KeyValues2 = _interopRequireDefault(_KeyValues);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return call && (typeof call === \"object\" || typeof call === \"function\") ? call : self; }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function, not \" + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }\n\nvar TitleScreen = function (_Container) {\n    _inherits(TitleScreen, _Container);\n\n    function TitleScreen() {\n        _classCallCheck(this, TitleScreen);\n\n        var _this = _possibleConstructorReturn(this, (TitleScreen.__proto__ || Object.getPrototypeOf(TitleScreen)).call(this));\n\n        _this.playingBGM = true;\n        _this.wordArray = [];\n\n        var cache = _pixi.utils.TextureCache;\n        _this.image = new _pixi.Sprite(cache[\"assets/images/Coin.png\"]);\n        _this.image2 = new _pixi.Sprite(cache[\"assets/images/SmallPac.png\"]);\n        _this.image2.x = 500;\n        _this.image2.y = 400;\n\n        _this.word = new PIXI.Text('', {\n            fontFamily: 'Arial',\n            fontSize: 35,\n            fill: 0x00FFFF,\n            align: 'center'\n        });\n        _this.word.position.set(200, 150);\n\n        _this.numbText = new PIXI.Text('Hello World', {\n            fontFamily: 'Arial',\n            fontSize: 35,\n            fill: 0x00FFFF,\n            align: 'center'\n        });\n        _this.numbText.position.set(450, 50);\n\n        _this.tutorText = new PIXI.Text('arrow keys to move coin,1-to mute bgm, 2orMouseclick-Play punch sfx, 3-play boing sfx, 4 or click SmallPacman-stop or playbgm', {\n            fontFamily: 'Arial',\n            fontSize: 15,\n            fill: 0x00FFFF,\n            align: 'center'\n        });\n        _this.tutorText.position.set(0, 450);\n\n        //Number5\n        _this.bgmInstance = createjs.Sound.play(\"assets/audio/WallE.mp3\", {\n            volume: 1.0,\n            loop: -1\n            //delay: 100 //ma\n        });\n\n        _this.sfxInstance1 = createjs.Sound.play(\"assets/audio/Punch.mp3\", {\n            volume: 1.0,\n            loop: 0\n            //delay: 100 //ma\n        });\n\n        _this.sfxInstance2 = createjs.Sound.play(\"assets/audio/Boing.mp3\", {\n            volume: 1.0,\n            loop: 0\n            //delay: 100 //ma\n        });\n\n        _this.image2.interactive = true;\n        _this.image2.on('pointerdown', _this.onClick.bind(_this));\n\n        _this.addChild(_this.numbText);\n        _this.addChild(_this.word);\n        _this.addChild(_this.tutorText);\n        _this.addChild(_this.image);\n        _this.addChild(_this.image2);\n\n        window.addEventListener(\"keyup\", _this.onKeyUp.bind(_this));\n        window.addEventListener(\"keydown\", _this.onKeyDown.bind(_this));\n        window.addEventListener(\"mousedown\", _this.mouseDown.bind(_this));\n        return _this;\n    }\n\n    _createClass(TitleScreen, [{\n        key: \"onClick\",\n        value: function onClick(a) {\n            //Number9\n            //this.bgmInstance.play = !this.bgmInstance.stop;\n            console.log(\"clicked\");\n            if (this.playingBGM) {\n                this.bgmInstance.stop();\n                this.playingBGM = false;\n            } else if (!this.playingBGM) {\n                this.bgmInstance.play();\n                this.playingBGM = true;\n            }\n        }\n    }, {\n        key: \"mouseDown\",\n        value: function mouseDown(m) {\n            //Number 7\n            this.sfxInstance1.play();\n        }\n    }, {\n        key: \"onKeyUp\",\n        value: function onKeyUp(e) {\n            //Number 1\n            this.numbText.text = e.key;\n            //Number3\n            if (e.key == \"Shift\") {\n                return;\n            }\n            this.wordArray.push(e.key);\n            //Number4 for some reason the pop or splice is not working\n            if (e.key == \"Backspace\") {\n                this.wordArray.pop();\n                console.log(\"b = \" + this.wordArray);\n                this.word.text = this.wordArray.join(\"\");\n                console.log(\"a = \" + this.wordArray);\n            }\n            console.log(\"b if = \" + this.wordArray);\n            this.word.text = this.wordArray.join(\"\");\n            console.log(\"a if = \" + this.wordArray);\n            //Number6\n            if (e.key == \"1\") {\n                this.bgmInstance.muted = !this.bgmInstance.muted;\n            }\n            if (e.key == \"2\") {\n                this.sfxInstance1.play();\n            }\n            if (e.key == \"3\") {\n                this.sfxInstance2.play();\n            }\n            //Number 8\n            if (e.key == \"4\") {\n                if (this.playingBGM) {\n                    this.bgmInstance.stop();\n                    this.playingBGM = false;\n                } else if (!this.playingBGM) {\n                    this.bgmInstance.play();\n                    this.playingBGM = true;\n                }\n            }\n        }\n    }, {\n        key: \"onKeyDown\",\n        value: function onKeyDown(a) {\n            //Number2\n            this.moveObj(this.image, a);\n        }\n    }, {\n        key: \"moveObj\",\n        value: function moveObj(obj, a) {\n            if (a.key == \"ArrowUp\") {\n                obj.y -= 5;\n            } else if (a.key == \"ArrowDown\") {\n                obj.y += 5;\n            } else if (a.key == \"ArrowLeft\") {\n                obj.x -= 5;\n            } else if (a.key == \"ArrowRight\") {\n                obj.x += 5;\n            }\n            if (obj.x >= 550) {\n                obj.x = 550;\n            }\n            if (obj.x <= 0) {\n                obj.x = 0;\n            }\n            if (obj.y >= 400) {\n                obj.y = 400;\n            }\n            if (obj.y <= 0) {\n                obj.y = 0;\n            }\n        }\n    }]);\n\n    return TitleScreen;\n}(_pixi.Container);\n\nexports.default = TitleScreen;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc2NyZWVuL1RpdGxlU2NyZWVuLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9zY3JlZW4vVGl0bGVTY3JlZW4uanM/YTM3MiJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb250YWluZXIsIFRleHQsVEVYVF9HUkFESUVOVCx0aWNrZXIsdXRpbHMsU3ByaXRlIH0gZnJvbSBcInBpeGkuanNcIjtcclxuaW1wb3J0IEtleUNvZGVzIGZyb20gXCIuLi9LZXlDb2Rlc1wiO1xyXG5pbXBvcnQgS2V5VmFsdWVzIGZyb20gXCIuLi9LZXlWYWx1ZXNcIjtcclxuXHJcbmNsYXNzIFRpdGxlU2NyZWVuIGV4dGVuZHMgQ29udGFpbmVye1xyXG4gICAgY29uc3RydWN0b3IoKXtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIHRoaXMucGxheWluZ0JHTSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy53b3JkQXJyYXkgPSBbXTtcclxuXHJcbiAgICAgICAgdmFyIGNhY2hlID0gdXRpbHMuVGV4dHVyZUNhY2hlO1xyXG4gICAgICAgIHRoaXMuaW1hZ2UgPSBuZXcgU3ByaXRlKGNhY2hlW1wiYXNzZXRzL2ltYWdlcy9Db2luLnBuZ1wiXSk7XHJcbiAgICAgICAgdGhpcy5pbWFnZTIgPSBuZXcgU3ByaXRlKGNhY2hlW1wiYXNzZXRzL2ltYWdlcy9TbWFsbFBhYy5wbmdcIl0pO1xyXG4gICAgICAgIHRoaXMuaW1hZ2UyLnggPSA1MDA7XHJcbiAgICAgICAgdGhpcy5pbWFnZTIueSA9IDQwMDtcclxuXHJcbiAgICAgICAgdGhpcy53b3JkID0gbmV3IFBJWEkuVGV4dCgnJyx7XHJcbiAgICAgICAgICAgIGZvbnRGYW1pbHk6ICdBcmlhbCcsXHJcbiAgICAgICAgICAgIGZvbnRTaXplOiAzNSxcclxuICAgICAgICAgICAgZmlsbDogMHgwMEZGRkYsXHJcbiAgICAgICAgICAgIGFsaWduOiAnY2VudGVyJ1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMud29yZC5wb3NpdGlvbi5zZXQoMjAwLDE1MCk7XHJcblxyXG4gICAgICAgIHRoaXMubnVtYlRleHQgPSBuZXcgUElYSS5UZXh0KCdIZWxsbyBXb3JsZCcse1xyXG4gICAgICAgICAgICBmb250RmFtaWx5OiAnQXJpYWwnLFxyXG4gICAgICAgICAgICBmb250U2l6ZTogMzUsXHJcbiAgICAgICAgICAgIGZpbGw6IDB4MDBGRkZGLFxyXG4gICAgICAgICAgICBhbGlnbjogJ2NlbnRlcidcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLm51bWJUZXh0LnBvc2l0aW9uLnNldCg0NTAsNTApO1xyXG5cclxuICAgICAgICB0aGlzLnR1dG9yVGV4dCA9IG5ldyBQSVhJLlRleHQoJ2Fycm93IGtleXMgdG8gbW92ZSBjb2luLDEtdG8gbXV0ZSBiZ20sIDJvck1vdXNlY2xpY2stUGxheSBwdW5jaCBzZngsIDMtcGxheSBib2luZyBzZngsIDQgb3IgY2xpY2sgU21hbGxQYWNtYW4tc3RvcCBvciBwbGF5YmdtJyx7XHJcbiAgICAgICAgICAgIGZvbnRGYW1pbHk6ICdBcmlhbCcsXHJcbiAgICAgICAgICAgIGZvbnRTaXplOiAxNSxcclxuICAgICAgICAgICAgZmlsbDogMHgwMEZGRkYsXHJcbiAgICAgICAgICAgIGFsaWduOiAnY2VudGVyJ1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMudHV0b3JUZXh0LnBvc2l0aW9uLnNldCgwLDQ1MCk7XHJcblxyXG4gICAgICAgIC8vTnVtYmVyNVxyXG4gICAgICAgIHRoaXMuYmdtSW5zdGFuY2UgPSBjcmVhdGVqcy5Tb3VuZC5wbGF5KFwiYXNzZXRzL2F1ZGlvL1dhbGxFLm1wM1wiLHtcclxuICAgICAgICAgICAgdm9sdW1lIDogMS4wLFxyXG4gICAgICAgICAgICBsb29wIDogLTFcclxuICAgICAgICAgICAgLy9kZWxheTogMTAwIC8vbWFcclxuICAgICAgICB9KVxyXG5cclxuICAgICAgICB0aGlzLnNmeEluc3RhbmNlMSA9IGNyZWF0ZWpzLlNvdW5kLnBsYXkoXCJhc3NldHMvYXVkaW8vUHVuY2gubXAzXCIse1xyXG4gICAgICAgICAgICB2b2x1bWUgOiAxLjAsXHJcbiAgICAgICAgICAgIGxvb3AgOiAwXHJcbiAgICAgICAgICAgIC8vZGVsYXk6IDEwMCAvL21hXHJcbiAgICAgICAgfSlcclxuXHJcbiAgICAgICAgdGhpcy5zZnhJbnN0YW5jZTIgPSBjcmVhdGVqcy5Tb3VuZC5wbGF5KFwiYXNzZXRzL2F1ZGlvL0JvaW5nLm1wM1wiLHtcclxuICAgICAgICAgICAgdm9sdW1lIDogMS4wLFxyXG4gICAgICAgICAgICBsb29wIDogMFxyXG4gICAgICAgICAgICAvL2RlbGF5OiAxMDAgLy9tYVxyXG4gICAgICAgIH0pXHJcblxyXG4gICAgICAgIHRoaXMuaW1hZ2UyLmludGVyYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmltYWdlMi5vbigncG9pbnRlcmRvd24nLHRoaXMub25DbGljay5iaW5kKHRoaXMpKTtcclxuXHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZCh0aGlzLm51bWJUZXh0KTtcclxuICAgICAgICB0aGlzLmFkZENoaWxkKHRoaXMud29yZCk7XHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZCh0aGlzLnR1dG9yVGV4dCk7XHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZCh0aGlzLmltYWdlKTtcclxuICAgICAgICB0aGlzLmFkZENoaWxkKHRoaXMuaW1hZ2UyKTtcclxuXHJcbiAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJrZXl1cFwiLHRoaXMub25LZXlVcC5iaW5kKHRoaXMpKTtcclxuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcImtleWRvd25cIix0aGlzLm9uS2V5RG93bi5iaW5kKHRoaXMpKTtcclxuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlZG93blwiLHRoaXMubW91c2VEb3duLmJpbmQodGhpcykpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uQ2xpY2soYSl7XHJcbiAgICAgICAgLy9OdW1iZXI5XHJcbiAgICAgICAgLy90aGlzLmJnbUluc3RhbmNlLnBsYXkgPSAhdGhpcy5iZ21JbnN0YW5jZS5zdG9wO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiY2xpY2tlZFwiKTtcclxuICAgICAgICBpZih0aGlzLnBsYXlpbmdCR00pXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aGlzLmJnbUluc3RhbmNlLnN0b3AoKTtcclxuICAgICAgICAgICAgdGhpcy5wbGF5aW5nQkdNID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2UgaWYoIXRoaXMucGxheWluZ0JHTSlcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRoaXMuYmdtSW5zdGFuY2UucGxheSgpO1xyXG4gICAgICAgICAgICB0aGlzLnBsYXlpbmdCR00gPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgIFxyXG4gICAgfVxyXG4gICAgbW91c2VEb3duKG0pe1xyXG4gICAgICAgIC8vTnVtYmVyIDdcclxuICAgICAgICB0aGlzLnNmeEluc3RhbmNlMS5wbGF5KCk7XHJcbiAgICB9XHJcblxyXG4gICAgb25LZXlVcChlKXtcclxuICAgICAgICAvL051bWJlciAxXHJcbiAgICAgICAgdGhpcy5udW1iVGV4dC50ZXh0ID0gZS5rZXk7XHJcbiAgICAgICAgLy9OdW1iZXIzXHJcbiAgICAgICAgaWYoZS5rZXkgPT0gXCJTaGlmdFwiKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLndvcmRBcnJheS5wdXNoKGUua2V5KTtcclxuICAgICAgICAvL051bWJlcjQgZm9yIHNvbWUgcmVhc29uIHRoZSBwb3Agb3Igc3BsaWNlIGlzIG5vdCB3b3JraW5nXHJcbiAgICAgICAgaWYoZS5rZXkgPT0gXCJCYWNrc3BhY2VcIilcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRoaXMud29yZEFycmF5LnBvcCgpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcImIgPSBcIit0aGlzLndvcmRBcnJheSk7XHJcbiAgICAgICAgICAgIHRoaXMud29yZC50ZXh0ID0gdGhpcy53b3JkQXJyYXkuam9pbihcIlwiKTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJhID0gXCIrdGhpcy53b3JkQXJyYXkpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zb2xlLmxvZyhcImIgaWYgPSBcIit0aGlzLndvcmRBcnJheSk7XHJcbiAgICAgICAgdGhpcy53b3JkLnRleHQgPSB0aGlzLndvcmRBcnJheS5qb2luKFwiXCIpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiYSBpZiA9IFwiK3RoaXMud29yZEFycmF5KTtcclxuICAgICAgICAvL051bWJlcjZcclxuICAgICAgICBpZihlLmtleSA9PSBcIjFcIilcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRoaXMuYmdtSW5zdGFuY2UubXV0ZWQgPSAhdGhpcy5iZ21JbnN0YW5jZS5tdXRlZDtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYoZS5rZXkgPT0gXCIyXCIpXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aGlzLnNmeEluc3RhbmNlMS5wbGF5KCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmKGUua2V5ID09IFwiM1wiKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgdGhpcy5zZnhJbnN0YW5jZTIucGxheSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvL051bWJlciA4XHJcbiAgICAgICAgaWYoZS5rZXkgPT0gXCI0XCIpXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBpZih0aGlzLnBsYXlpbmdCR00pXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYmdtSW5zdGFuY2Uuc3RvcCgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wbGF5aW5nQkdNID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSBpZighdGhpcy5wbGF5aW5nQkdNKVxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmJnbUluc3RhbmNlLnBsYXkoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMucGxheWluZ0JHTSA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25LZXlEb3duKGEpe1xyXG4gICAgICAgIC8vTnVtYmVyMlxyXG4gICAgICAgIHRoaXMubW92ZU9iaih0aGlzLmltYWdlLGEpO1xyXG4gICAgfVxyXG5cclxuICAgIG1vdmVPYmoob2JqLGEpe1xyXG4gICAgICAgIGlmKGEua2V5ID09IFwiQXJyb3dVcFwiKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgb2JqLnkgLT0gNTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSBpZihhLmtleSA9PSBcIkFycm93RG93blwiKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgb2JqLnkgKz0gNTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSBpZihhLmtleSA9PSBcIkFycm93TGVmdFwiKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgb2JqLnggLT0gNTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSBpZihhLmtleSA9PSBcIkFycm93UmlnaHRcIilcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIG9iai54ICs9IDU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmKG9iai54ID49IDU1MClcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIG9iai54ID0gNTUwO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZihvYmoueCA8PSAwKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgb2JqLnggPSAwO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZihvYmoueSA+PSA0MDApXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBvYmoueSA9IDQwMDtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYob2JqLnkgPD0gMClcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIG9iai55ID0gMDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFRpdGxlU2NyZWVuOyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQTtBQUNBO0FBQUE7QUFDQTs7O0FBQUE7QUFDQTs7Ozs7Ozs7Ozs7QUFDQTs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBakVBO0FBa0VBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFFQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBR0E7QUFDQTtBQUdBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7Ozs7QUFqTEE7QUFDQTtBQW1MQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/screen/TitleScreen.js\n");

/***/ }),

/***/ "Config":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** external "{\"BUILD\":{\"GAME_TITLE\":\"GAME TITLE\",\"VERSION\":\"0.0.1\",\"WIDTH\":889,\"HEIGHT\":500,\"MINIFIED_EXTERNAL_JS\":false,\"MINIFIED_EXTERNAL_JS_PATH\":\"./src/external/minified\",\"UNMINIFIED_EXTERNAL_JS_PATH\":\"./src/external/unminified\",\"EXTERNAL_JS\":[\"soundjs.js\",\"preloadjs.js\"],\"ASSETPATH\":{\"load\":\"assets\",\"audio\":\"assets/audio\"}}}" ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = {\"BUILD\":{\"GAME_TITLE\":\"GAME TITLE\",\"VERSION\":\"0.0.1\",\"WIDTH\":889,\"HEIGHT\":500,\"MINIFIED_EXTERNAL_JS\":false,\"MINIFIED_EXTERNAL_JS_PATH\":\"./src/external/minified\",\"UNMINIFIED_EXTERNAL_JS_PATH\":\"./src/external/unminified\",\"EXTERNAL_JS\":[\"soundjs.js\",\"preloadjs.js\"],\"ASSETPATH\":{\"load\":\"assets\",\"audio\":\"assets/audio\"}}};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29uZmlnLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwie1xcXCJCVUlMRFxcXCI6e1xcXCJHQU1FX1RJVExFXFxcIjpcXFwiR0FNRSBUSVRMRVxcXCIsXFxcIlZFUlNJT05cXFwiOlxcXCIwLjAuMVxcXCIsXFxcIldJRFRIXFxcIjo4ODksXFxcIkhFSUdIVFxcXCI6NTAwLFxcXCJNSU5JRklFRF9FWFRFUk5BTF9KU1xcXCI6ZmFsc2UsXFxcIk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcXFwiOlxcXCIuL3NyYy9leHRlcm5hbC9taW5pZmllZFxcXCIsXFxcIlVOTUlOSUZJRURfRVhURVJOQUxfSlNfUEFUSFxcXCI6XFxcIi4vc3JjL2V4dGVybmFsL3VubWluaWZpZWRcXFwiLFxcXCJFWFRFUk5BTF9KU1xcXCI6W1xcXCJzb3VuZGpzLmpzXFxcIixcXFwicHJlbG9hZGpzLmpzXFxcIl0sXFxcIkFTU0VUUEFUSFxcXCI6e1xcXCJsb2FkXFxcIjpcXFwiYXNzZXRzXFxcIixcXFwiYXVkaW9cXFwiOlxcXCJhc3NldHMvYXVkaW9cXFwifX19XCI/ZDE0YiJdLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHtcIkJVSUxEXCI6e1wiR0FNRV9USVRMRVwiOlwiR0FNRSBUSVRMRVwiLFwiVkVSU0lPTlwiOlwiMC4wLjFcIixcIldJRFRIXCI6ODg5LFwiSEVJR0hUXCI6NTAwLFwiTUlOSUZJRURfRVhURVJOQUxfSlNcIjpmYWxzZSxcIk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcIjpcIi4vc3JjL2V4dGVybmFsL21pbmlmaWVkXCIsXCJVTk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcIjpcIi4vc3JjL2V4dGVybmFsL3VubWluaWZpZWRcIixcIkVYVEVSTkFMX0pTXCI6W1wic291bmRqcy5qc1wiLFwicHJlbG9hZGpzLmpzXCJdLFwiQVNTRVRQQVRIXCI6e1wibG9hZFwiOlwiYXNzZXRzXCIsXCJhdWRpb1wiOlwiYXNzZXRzL2F1ZGlvXCJ9fX07Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///Config\n");

/***/ })

/******/ });